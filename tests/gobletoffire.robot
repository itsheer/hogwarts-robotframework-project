*** Settings ***
Documentation     A test suite for the Alohomora API.
...
...               Keywords are imported from the resource file
Library     Collections
Library     String
Library     RequestsLibrary
Library     OperatingSystem
Resource    keywords.resource

Suite Setup     Setup Flask Http Server
Suite Teardown  Teardown Flask Http Server

*** Variables ***
${api}      gobletoffire

*** Test Cases ***

Verify Version Request
    [Tags]  version
    ${response} =    GET  ${HTTP_LOCAL_SERVER}/version
    Should Be Equal As Strings  ${response.status_code}  200
    Should Be Equal As Strings  ${response.json()['version']}  0.0.1


No Age in Request
    [Tags]  post
    ${json}=    Create Dictionary    name=Cedric    school=Hogwarts
    ${response}=    POST  ${HTTP_LOCAL_SERVER}/${api}/place    json=${json}
    Should Be Equal As Strings  ${response.status_code}  200
    Should Be Equal As Strings  ${response.json()['application_state']}  not accepted
Test Valid age
    [Documentation]    Test placing a valid candidate in the Goblet of Fire
    [Tags]    Place
    ${json}=    Create Dictionary    age=17    school=Hogwarts    name=Harry
    ${response}=    POST Request    ${HTTP_LOCAL_SERVER}/${api}/place    json=${json}
    Should Be Equal As Strings    ${response.status_code}    200
    Should Contain    ${response.text}    "accepted"

Test Invalid Age
    [Documentation]    Test placing a candidate with an invalid age in the Goblet of Fire
    [Tags]    Place
    ${json}=    Create Dictionary    age=15    school=Hogwarts    name=Ron
    ${response}=    POST Request    ${HTTP_LOCAL_SERVER}/${api}/place    json=${json}
    Should Be Equal As Strings    ${response.status_code}    200
    Should Contain    ${response.text}    "not accepted"

Test Invalid School
    [Documentation]    Test placing a candidate from an invalid school in the Goblet of Fire
    [Tags]    Place
    ${json}=    Create Dictionary    age=18    school=Beauxbatos    name=Fleur
    ${response}=    POST Request    ${HTTP_LOCAL_SERVER}/${api}/place    json=${json}
    Should Be Equal As Strings    ${response.status_code}    200
    Should Contain    ${response.text}    "not accepted"

Test Reveal Candidates
    [Documentation]    Test revealing chosen candidates from the Goblet of Fire
    [Tags]    Reveal
    ${response}=    POST Request    ${HTTP_LOCAL_SERVER}/${api}/reveal
    Should Be Equal As Strings    ${response.status_code}    200
    Should Contain    ${response.text}    "Hogwarts"
    Should Contain    ${response.text}    "Durmstrang"
    Should Contain    ${response.text}    "Beauxbatons"
Correct Tri-Wizard Tournament Application
    [Tags]  post
    ${json}=    Create Dictionary    name=Cedric    age=17     school=Hogwarts
    ${response}=    POST  ${HTTP_LOCAL_SERVER}/${api}/place    json=${json}
    Should Be Equal As Strings  ${response.status_code}  200
    Should Be Equal As Strings  ${response.json()['application_state']}  accepted
