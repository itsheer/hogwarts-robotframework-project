*** Settings ***
Documentation     A test suite for the Alohomora API.
...
...               Keywords are imported from the resource file
Library     Collections
Library     String
Library     RequestsLibrary
Library     OperatingSystem
Resource    keywords.resource

Suite Setup     Setup Flask Http Server
Suite Teardown  Teardown Flask Http Server

*** Variables ***
${api}      alohomora
@{numbers}  7    11    8    14    21    77
${MAX_CONCURRENCY}    10

*** Test Cases ***

Verify Version Request
    [Tags]  version

    ${response} =    GET  ${HTTP_LOCAL_SERVER}/version
    Should Be Equal As Strings  ${response.status_code}  200
    Should Be Equal As Strings  ${response.json()['version']}  0.0.1

Check Divisibility
    [Tags]  alohomora
    FOR    ${number}    IN    @{numbers}
        ${response} =    GET  ${HTTP_LOCAL_SERVER}/${api}/${number}
        Should Be Equal As Strings  ${response.status_code}  200

       Run Keyword If    ${number} % 7 == 0 and ${number} % 11 == 0
       Should Be Equal As Strings  ${response.text}  alohomora
        ...    ELSE IF    ${number} % 7 == 0
            Should Be Equal As Strings  ${response.text}  aloho
        ...    ELSE IF    ${number} % 11 == 0
            Should Be Equal As Strings  ${response.text}  mora
        ...    ELSE
            Should Be Equal As Strings  ${response.text}  ${number}
    END 
END





