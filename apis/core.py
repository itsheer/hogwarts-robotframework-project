from flask import Flask, request, jsonify, json 
from random import getrandbits

app = Flask(__name__)


class Candidate(object):

    def __init__(self, object):
        self.name = object["name"]
        self.school = object["school"]
        self.age = int(object["age"])


class GobletOfFire(object):
    
    def __init__(self):
        self.candidates = {}
    
    def place(self, candidate):
        """Add the person to the goblet. The function returns True if the 
        application was accepted, and false if the person did not qualify.
        True does not mean that the person will be chosen, it just means that
        his application met all the requirements"""
        
        if candidate.age < 17:
            return False
        
        valid_schools = ["Hogwarts", "Durmstrang", "Beauxbatons"]
        if candidate.school not in valid_schools:
            return False
        
        school = candidate.school
        if school not in self.candidates:
            self.candidates[school] = candidate.name
        return True
        
    def reveal(self):
        """Show the chosen candidates. After this function is executed, the
        Goblet of Fire is empty again and can be re-used for the next Tri-Wizard
        Championship."""
        ret = self.candidates
        self.candidates = {}
        return ret


# Ouch global state! Luckily this isn't production code!
# This also means that multithreaded access will comletely
# mess up the Goblet's state. 
GOBLET = GobletOfFire()

@app.route('/', methods=['GET'])
def home():
    return "Hello there!"

@app.route('/version', methods=['GET'])
def version():
    return  jsonify(
        version="0.0.1"
        )


@app.route('/alohomora/<int:number>',methods= ['GET'])
def alohomora(number):
    if number % 7 == 0 and number % 11 == 0:
        return 'alohomora'
    elif number % 7 == 0:
        return 'aloho'
    elif number % 11 == 0:
        return 'mora'
    else:
        return str(number)


@app.route('/gobletoffire/place', methods=['POST'])
def place():
    data = json.loads(request.data)

    if any (True for e in ("age", "school", "name") if e not in data):
        return jsonify(application_state="not accepted")
    try:
        candidate = Candidate(data)
        accepted = GOBLET.place(candidate)
    except:
        accepted = False

    return jsonify(
        application_state="accepted" if accepted else "not accepted"
    )

@app.route('/gobletoffire/reveal', methods=['POST'])
def reveal():
    candidates = GOBLET.reveal()
    return jsonify(candidates)


#app.run(host="127.0.0.1", port="5000")

if __name__ == "__main__":
    app.run()