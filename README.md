# The Hogwarts RobotFramework Project

In this project, we will introduce [RobotFramework](https://robotframework.org) to Albus Dumbledore. If only he had known about it during the last Tri-Wizard tournament, things might have been completely different.

What you will have to do in concrete is
1. Implement the Alohomora API call in the api/core.py file.
2. Work out the tests that are in the tests folder

## 1. Project Structure
This project consists of the following files and folders
- api: a folder that contains a small python Flask server in the apis folder. It is the APIs exposed by this server that need to be tested
- tests: a folder that contains two [RobotFramework](https://robotframework.org) test suites that need to be worked out.
- requirements.txt: a file that can be used to install dependencies

## 2. Installation
Python is needed to run the scripts in this project. The missing packages can be installed using pip and by passing the requirements.txt file (`pip install -r requirements.txt`). There are no particular OS restrictions.

## 3. The Alohomora API
Alohomora is a game where the small wizards take turns to count incrementally in front of a locked door, and replace any number divisible by seven with the word "aloho", and any number divisible by eleven with the word "mora". It is rumoured that the game is even more fun when played in front of the door of Argus Filch, and certainly when multiples of seven and eleven are involved!

so the game would go like this:
1, 2, 3, 4, 5, 6, aloho, 8, 9, 10, mora, 12, ...

The Alohomora API in our web server is doing this, for example
```
http://localhost:5000/alohomora/7
```
will reply the text `aloho`.

```
http://localhost:5000/alohomora/3
```
will reply the text `3`.


The API only accepts HTTP GET requests and returns text, not json. Invalid input should not trigger the underlying python method.

## 4. The Goblet of Fire API
The Goblet of Fire is a tool that is used to select the best candidates for each school for the Tri-Wizard tournament. The API consists of two separate sub-APIs: `place` and `reveal.

### 4.1 place
Candidates can place their name in the goblet, but the goblet verifies whether the candidate is admissable or not. The rules for admissability are
1. The candidate should be at least 17 years old
2. The candidate should be a student from either Hogwarts, Durmstrang or Beauxbatons.

This part of the API can be accessed using a HTTP POST request to:
```
http://localhost:5000/gobletoffire/place
```
The json that is sent needs to be an object that contains a name, a school and an age. An example of a valid place request is
```
curl -X POST http://localhost:5000/goblet/place -H 'Content-Type: application/json' -d '{"age":17 ,"school":"durmstrang", "name":"victor"}'
```
The responds is a json object 
```
{"application_state":"accepted"}
```
where `application_state` will be `accepted` when candidate met all the requirements, otherwise `application_state` will be `not accepted` (for example, when the candidate is too young).

> The place API contains errors against this rule. This is deliberate and this will mean that if aftewards you implement the tests, some tests should fail, to indicate what part of this API's implementation needs improvement.

The Goblet of Fire will be emptied only when the web server is restarted, or when the reveal API is called.

### 4.2 reveal
The Goblet of Fire can also be ordered to reveal the candidates he has chosen. After this is done, the goblet of fire will be empty. The result of a reveal will be JSON object that contains the for every school the respective chosen candidate.
```
curl -X POST http://localhost:5000/goblet/reveal
```
where a possible response is
```
{"Durmstrang":"Victor Krum","Hogwarts":"Cedric Diggory"}
```
Here, the chosen candidate for the school Durmstrang is Viktor Krum and Cedric Diggory is chosen for Hogwarts.

> The Goblet of Fire's selection algorithm is non-deterministic. Keep this in mind when designing tests.

> The Goblet of Fire API will not work properly if multiple calls will be made in parallel. This should not give any problems for the current case.

## 5. How to Manually Test the API
The APIs are implemented using an extremely simple [python Flask](https://flask.palletsprojects.com/en/3.0.x/) server and can be started as any regular Flask server can. By default, it will be listening on port 5000 on localhost.

Once the server is running, you can open your web browser and go to http://localhost:5000/ and should see the message "Hello there!". The Alohomora api can be tested entirely using the webbrowser. The gobletoffire API can be more easily tested using tools like for example curl:
```
❯ curl -X POST http://localhost:5000/goblet/place -H 'Content-Type: application/json' -d '{"age":17 ,"school":"durmstrang", "name":"victor"}'
{"application_state":"accepted"}
❯ curl -X POST http://localhost:5000/goblet/place -H 'Content-Type: application/json' -d '{"age":17 ,"school":"hogwarts", "name":"Harry"}'
{"application_state":"accepted"}
❯ curl -X POST http://localhost:5000/goblet/reveal
{"durmstrang":"victor","hogwarts":"Harry"}
```

## 6. How to Execute a Test Suite

Executing a single test suite is fairly easy. From the command line, execute 
```sh
robot test/alohomora.py
```
or---if you want to tests the gobletoffire API--, execute instead:
```sh
robot test/gobletoffire.py
```

> The Flask server will be automatically spun up when executing a test suite and will be closed when the test suite is finished. No need to worry about that.

A small test report should show up that says what test passed.

## 7. Problem 1
Implement the Alohomora API in the `api/core.py` file. There is already a function
`alohomora` in place, but its implementation is empty and the decorator for the function is missing.

Add the proper decorator such that only post requests of the correct form are routed to this function.

Implement the `alohomora` function so that the Alohomora API replies as explained in the Alohomora API section (par. 3).

## 8. Problem 2
Work out tests for the Alohomora API in `tests/alohomora.robot` file. Think about edge cases!

## 9. Problem 3
Work out a complete as possible Test Suite for the Goblet of Fire API. This is to be done in the `tests/gobletoffire.robot` file.

The api/core.sh should not be improved or changed. This exercises is about testing.


## 10. General Remarks
The idea is also to show how fast you can work yourself into a framework you don't know. This means that the point is not just to get something working, but to try to get an elegant solution, by using features that come with the framework. Or to enhance the current Test Suite. Examples of improvements are the use of tags, proper use of variables, loops, ... 

In principle, no extra external RobotFramework libraries need to be added, unless you have a really good reason of course.